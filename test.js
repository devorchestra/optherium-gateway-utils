const {describe, it, before, after} = require('mocha');
const chai = require('chai');

let utils = require('./index');

const { assert } = chai;

describe('utils.checkAPICompatibility', () => {
    it('checks API version in right way', (done) => {
        assert.isFalse(utils.checkAPICompatibility('1.2.3', '2.2.2'));
        assert.isFalse(utils.checkAPICompatibility('1.2.3', '1.2.4'));
        assert.isFalse(utils.checkAPICompatibility('1.2.3', '1.3.2'));
        assert.isTrue(utils.checkAPICompatibility('1.2.3', '1.2.3'));
        assert.isTrue(utils.checkAPICompatibility('1.2.3', '1.1.4'));
        assert.isTrue(utils.checkAPICompatibility('1.2.3', '1.2.0'));
        done();
    })
});