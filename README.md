# optherium-gateway-utils

A set of auxiliary methods for API gateways services that are used in Optherium microservice architecture with strong binding to AWS SQS as message broker.

## How to use
See index.d.ts types file 

## Documentation
You can use `typedoc` to generate documentation of the module:
```
$ typedoc --out docs/ --includeDeclarations index.d.ts
```
