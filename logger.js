let winston = require('winston');
let moment = require('moment');

module.exports = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            level: process.env.GATEWAY_LOG_LEVEL || 'info',
            timestamp: () => { return moment().toISOString() },
            formatter: (options) => {
                return `${options.timestamp()} gateway-utils ${options.level.toUpperCase()} ${options.message || ''}`
            },
            stderrLevels: ['error']
        })
    ]
})