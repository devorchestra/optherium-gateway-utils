let logger = require('./logger');
let Promise = require('bluebird');
let generateUuid = require('uuid/v4');

exports.startListenResponses = function(broker, messageBus, config) {
    return Promise
        .resolve()
        .then(() => {
            if (config.environment === 'development') {
                return;
            }
            return createGatewayQueue(broker, config);
        })
        .then(() => {
            broker.process(`${config.sqs.gatewayResponseSubject}`, {}, ({meta, payload}, done) => {
                let {responseSubject, requestId} = meta;
                logger.debug(`Processing response with subject ${responseSubject} with response id ${requestId}`);
                messageBus.emit(`${responseSubject}.${requestId}`, payload);
                done();
            });
        });
}

exports.waitResponse = function(messageBus, subject, timeout, handler) {
    let timeoutId = setTimeout(() => {
        messageBus.removeAllListeners(subject);
        handler({
            success: false,
            status: 408,
            errorMessage: 'request timeout'
        });
    }, timeout);

    messageBus.once(subject, (data) => {
        clearTimeout(timeoutId);
        handler(data);
    });
}

exports.addPOSIXSignalsListeners = function(broker, config) {
    process.on('SIGINT', () => {cleanUpAndShutDown(1, broker, config)});
    process.on('SIGTERM', () => {cleanUpAndShutDown(1, broker, config)});
    process.on('uncaughtException', function(e) {
        logger.error('Uncaught exception...');
        logger.error(e.stack);
        process.exit(1);
    });
    process.on('exit', (code) => {
        logger.info(`Exiting with code ${code}`);
    })
}

exports.cleanUpAndShutDown = cleanUpAndShutDown;

exports.checkAPICompatibility = function (appVersion, minAppVersion) {
    let appVersionNumbers = appVersion.split('.');
    let minAppVersionNumbers = minAppVersion.split('.');
    let isCompatible = true;
    for (let i = 0; i < appVersionNumbers.length; i++) {
        if (appVersionNumbers[i] > minAppVersionNumbers[i]) {
            isCompatible = true;
            break;
        } else if (appVersionNumbers[i] < minAppVersionNumbers[i]) {
            isCompatible = false;
            break;
        }
    }
    return isCompatible;
};

/**
 * auxiliary functions
 */

function cleanUpAndShutDown(code, broker, config) {
    // NOTHING TO CLEAN UP
    if (!config || !broker || (config.environment === 'development')) return process.exit(code);

    // DELETING GATEWAY QUEUE
    broker.deleteQueue(config.sqs.gatewayResponseSubject, {env: config.environment}, () => {
        process.exit(code)
    });
}

function createGatewayQueue(broker, config) {
    return new Promise((resolve, reject) => {
        let queueName = config.sqs.gatewayResponseSubject + '-' + generateUuid();
        let options = {env: config.environment};
        if (config.sqs.redrivePolicy) options.redrivePolicy = config.sqs.redrivePolicy;
        broker.createQueue(queueName, options, (error) => {
            if (error) return reject(error);
            config.sqs.gatewayResponseSubject = queueName;
            resolve();
        })
    })
        .catch((error) => {
            if (error.status && (error.status === 409)) {
                logger.error(`${error.message}. Retrying ...`);
                return createGatewayQueue(broker, config);
            }
            throw error;
        })
}