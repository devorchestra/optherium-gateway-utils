
// @ts-ignore
@deprecated
export function startListenResponses(broker: any, messageBus: any, config: any): Promise<any>;

// @ts-ignore
@deprecated
export function waitResponse(messageBus: any, subject: string, timeout: number, handler: any): void;

/**
 * Add "clean up" listeners for shut down signals
 *
 * @param broker- instance of SQS message broker (see Optherium's aws-sqs-wrapper module)
 * @param config - service configuration object
 */
export function addPOSIXSignalsListeners(broker: any, config: any): void;

/**
 * Clean up used resources before shut down
 *
 * @param {number} code - shutdown code
 * @param broker - instance of SQS message broker (see Optherium's aws-sqs-wrapper module)
 * @param config - service configuration object
 */
export function cleanUpAndShutDown(code: number, broker: any, config: any): void;

/**
 * Check compatibility of API (gateway) with client app
 *
 * @param {string} appVersion - version of the client app (e.x. 1.4.11)
 * @param {string} minAppVersion - min version of the client app that this Gateway supports
 *
 * @return {boolean} isCompatible - indicates are gateway and app compatible
 */
export function checkAPICompatibility(appVersion: string, minAppVersion: string): boolean;